import { css, html, LitElement } from "lit";
import { customElement, property, state } from "lit/decorators";
import { ULDKApi } from "./ULDKApi";
import "./uldk-mouse";
import "./uldk-parcel";
import L from "leaflet";

enum Settings {
  ObjectId,
  ParcelId,
  Mouse,
  Home,
}

@customElement("uldk-controller")
export class UldkController extends LitElement {
  @state() currentSetting: Settings = Settings.Home;

  @state() api: ULDKApi = new ULDKApi();

  @property({ type: Object }) map?: L.Map;

  @property({ type: Object }) geojsonLayer?: any;

  static styles = css`
    :host {
      position: absolute;
      top: 10px;
      right: 10px;
      padding: 10px;
      background-color: white;
      width: 270px;
      overflow: auto;
    }

    vaadin-button {
      width: 100%;
    }

    h3 {
      text-align: center;
    }
  `;

  private changeSettings(setting: Settings) {
    this.currentSetting = setting;
  }

  primaryNavigation() {
    return html`
      <vaadin-button
        theme="primary"
        @click=${() => {
          this.changeSettings(Settings.Home);
          this.map?.removeEventListener("click");
          this.geojsonLayer.clearLayers();
        }}
      >
        Cofnij
      </vaadin-button>
    `;
  }

  optionButtons() {
    return html`
      <h3>Wyszukaj działkę</h3>
      <vaadin-button
        theme="primary"
        @click=${() => this.changeSettings(Settings.ObjectId)}
      >
        Pełny adres
      </vaadin-button>
      <vaadin-button
        theme="primary"
        @click=${() => this.changeSettings(Settings.ParcelId)}
      >
        Id działki
      </vaadin-button>
      <vaadin-button
        theme="primary"
        @click=${() => this.changeSettings(Settings.Mouse)}
      >
        Wskasz myszką
      </vaadin-button>
    `;
  }

  optionObjectId() {
    return html`
      ${this.primaryNavigation()}
      <uldk-panel
        .api=${this.api}
        .geojsonLayer=${this.geojsonLayer}
        .map=${this.map}
      >
      </uldk-panel>
    `;
  }

  optionMouseClick() {
    return html`
      ${this.primaryNavigation()}
      <uldk-mouse
        id="mouse-option-id"
        .api=${this.api}
        .geojsonLayer=${this.geojsonLayer}
        .map=${this.map}
      >
      </uldk-mouse>
    `;
  }

  optionParcel() {
    return html`
      ${this.primaryNavigation()}
      <uldk-parcel
        .api=${this.api}
        .geojsonLayer=${this.geojsonLayer}
        .map=${this.map}
      >
      </uldk-parcel>
    `;
  }

  render() {
    if (this.currentSetting === Settings.Home) {
      return this.optionButtons();
    } else if (this.currentSetting === Settings.ObjectId) {
      return this.optionObjectId();
    } else if (this.currentSetting === Settings.Mouse) {
      return this.optionMouseClick();
    } else if (this.currentSetting === Settings.ParcelId) {
      return this.optionParcel();
    }
  }
}
