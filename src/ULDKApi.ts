export class ULDKApi {
  private static baseUrl: string = "https://uldk.gugik.gov.pl/";

  async getAdministrativeNames(
    param: string,
    name: string,
    teryt: string = ""
  ): Promise<string[]> {
    const url = `${ULDKApi.baseUrl}?request=${param}&result=${name},teryt&id=${teryt}`;
    const text = await fetch(url).then((r) => r.text());
    const result = text.substring(1).trim();
    return result.split("\n");
  }

  async getGeoJsonFromParcelId(
    param: string,
    name: string,
    teryt: string
  ): Promise<string> {
    const url = `${ULDKApi.baseUrl}?request=${param}&result=${name},voivodeship,county,commune,region,id,teryt&srid=4326&id=${teryt}`;
    const text = await fetch(url).then((r) => r.text());

    return this.#processResponse(text);
  }

  async getGeoJsonFromPoint(x: string, y: string): Promise<string> {
    const url = `${ULDKApi.baseUrl}?request=GetParcelByXY&xy=${y},${x},4326&result=geom_wkt,voivodeship,county,commune,region,id,teryt&srid=4326`;
    const text = await fetch(url).then((r) => r.text());

    return this.#processResponse(text);
  }

  async getGeoJsonFromParcelIdOrNr(id: string): Promise<string> {
    const url = `${ULDKApi.baseUrl}?request=GetParcelByIdOrNr&id=${id}&result=geom_wkt,voivodeship,county,commune,region,id,teryt&srid=4326`;
    const text = await fetch(url).then((r) => r.text());

    return this.#processResponse(text);
  }

  #processResponse(text: string) {
    const result = text.substring(1).trim();
    const wkt = (result.includes(";") ? result.split(";")[1] : result)
      ?.trim()
      .split("\n")[0];

    return wkt;
  }
}
