import "@vaadin/vaadin-button";
import "@vaadin/vaadin-combo-box";
import "@vaadin/vaadin-text-field";
import L from "leaflet";
import { css, html, LitElement } from "lit";
import { customElement, property, query, state } from "lit/decorators.js";
import wellknown from "wellknown/wellknown.js";
import { Helper } from "./Helper";
import { ULDKApi } from "./ULDKApi";

interface uldkItem {
  name: string;
  teryt: string;
}
@customElement("uldk-panel")
export class SimpleGreeting extends LitElement {
  // Define scoped styles right with your component, in plain CSS
  static styles = css`
    vaadin-combo-box {
      width: 100%;
    }

    vaadin-text-field {
      width: 100%;
    }
  `;

  @property({ type: Object }) map?: L.Map;
  @property({ type: Object }) api?: ULDKApi;
  @property({ type: Object }) geojsonLayer?: any;

  @state() search_types_by_option = {
    Wojewodztwo: {
      param: "GetVoivodeshipById",
      name: "voivodeship",
    },
    Powiat: {
      param: "GetCountyById",
      name: "county",
    },
    Gmina: {
      param: "GetCommuneById",
      name: "commune",
    },
    Region: {
      param: "GetRegionById",
      name: "region",
    },
    Dzialka: {
      param: "GetParcelById",
      name: "geom_wkt",
    },
  };

  @query("#voivodeship")
  voivodeshipNode: any;

  @query("#county")
  countyNode: any;

  @query("#commune")
  communeNode: any;

  @query("#region")
  regionNode: any;

  @query("#parcelNr")
  parcelInput: any;

  firstUpdated(props: any) {
    super.firstUpdated(props);
  }

  wktToGeoJSON(wkt: string): GeoJSON.GeometryObject {
    return wellknown.parse(wkt);
  }

  async getAdministrativeNames(type: string, teryt: string = "") {
    const param = this.search_types_by_option[type].param;
    const name = this.search_types_by_option[type].name;
    const result = await this.api?.getAdministrativeNames(param, name, teryt);
    let items: uldkItem[] = [];

    result?.forEach((item) => {
      const itemSplit = item.split("|");
      items.push({ name: itemSplit.join(" | "), teryt: itemSplit[1] });
    });

    return items;
  }

  async getParcelById(type: string, teryt: string = "") {
    const param = this.search_types_by_option[type].param;
    const name = this.search_types_by_option[type].name;
    const result = await this.api?.getGeoJsonFromParcelId(param, name, teryt);

    const wktJSON = Helper.wktToGeoJSON(result!);
    const message = Helper.createInfoObject(result!);

    const dataJSON = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: wktJSON,
          properties: {
            popupContent: message,
            underConstruction: false,
          },
          id: 1,
        },
      ],
    };

    this.geojsonLayer?.clearLayers();
    this.geojsonLayer?.addData(dataJSON);

    this.map?.fitBounds(this.geojsonLayer?.getBounds(), {});

    // const arr = result.split("\n");

    // let items: uldkItem[] = [];

    // arr.forEach((item) => {
    //   const itemSplit = item.split("|");
    //   items.push({ name: itemSplit[1], teryt: itemSplit[0] });
    // });

    return "";
  }

  render() {
    return html`
      <h4>Pobieranie działek</h4>
      <vaadin-combo-box
        id="voivodeship"
        label="Wybierz województwo"
        clear-button-visible
        item-label-path="name"
        item-value-path="teryt"
        @selected-item-changed=${(e) => {
          this.countyNode.value = "";
          this.countyNode.items = [];
          this.countyNode.selectedItem = undefined;
        }}
        .dataProvider=${async (params, callback) => {
          let { filter } = params;

          let data = await this.getAdministrativeNames("Wojewodztwo");
          callback(data, data.length);
        }}
        @change=${async (e) => {
          this.countyNode.items = await this.getAdministrativeNames(
            "Powiat",
            e.target.value
          );
        }}
      ></vaadin-combo-box>
      <vaadin-combo-box
        id="county"
        label="Wybierz powiat"
        clear-button-visible
        item-label-path="name"
        item-value-path="teryt"
        @selected-item-changed=${(e) => {
          this.communeNode.value = "";
          this.communeNode.items = [];
          this.communeNode.selectedItem = undefined;
        }}
        @change=${async (e) => {
          this.communeNode.items = await this.getAdministrativeNames(
            "Gmina",
            e.target.value
          );
        }}
      ></vaadin-combo-box>
      <vaadin-combo-box
        id="commune"
        label="Wybierz gminę"
        clear-button-visible
        item-label-path="name"
        item-value-path="teryt"
        @selected-item-changed=${(e) => {
          this.regionNode.value = "";
          this.regionNode.items = [];
          this.regionNode.selectedItem = undefined;
        }}
        @change=${async (e) => {
          this.regionNode.items = await this.getAdministrativeNames(
            "Region",
            e.target.value
          );
        }}
      ></vaadin-combo-box>
      <vaadin-combo-box
        id="region"
        label="Wybierz region"
        clear-button-visible
        item-label-path="name"
        item-value-path="teryt"
      ></vaadin-combo-box>
      <vaadin-text-field
        id="parcelNr"
        label="Podaj nr działki"
      ></vaadin-text-field>
      <vaadin-button
        id="searchBtn"
        @click=${async (e) => {
          const teryt = `${this.regionNode.value}.${this.parcelInput.value}`;
          console.log(await this.getParcelById("Dzialka", teryt));
        }}
        >Szukaj w ULDK</vaadin-button
      >
    `;
  }
}

// request o kliknięcie na mapie
//https://uldk.gugik.gov.pl/?request=GetParcelByXY&result=teryt,region,voivodeship,geom_wkt&xy=23.0890058083815,52.0452642329098,4326
