import L from "leaflet";
import { html, LitElement } from "lit";
import { customElement, property, state } from "lit/decorators";
import { Helper } from "./Helper";
import { ULDKApi } from "./ULDKApi";

@customElement("uldk-mouse")
export class UldkMouse extends LitElement {
  @property({ type: Object }) api?: ULDKApi;

  @property({ type: Object }) map?: L.Map;

  @property({ type: Object }) geojsonLayer?: any;

  async getParcelById(x: string, y: string) {
    const result = await this.api?.getGeoJsonFromPoint(x, y);

    const wktJSON = Helper.wktToGeoJSON(result!);
    const message = Helper.createInfoObject(result!);

    const dataJSON = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: wktJSON,
          properties: {
            popupContent: message,
            underConstruction: false,
          },
          id: 1,
        },
      ],
    };

    this.geojsonLayer.clearLayers();
    this.geojsonLayer?.addData(dataJSON);

    this.map?.fitBounds(this.geojsonLayer.getBounds(), {});
  }

  firstUpdated(props: any) {
    this.mouseClickOption();
  }

  mouseClickOption() {
    this.map!.on("click", <LeafletMouseEvent>(e) => {
      var coord = e.latlng;
      var lat = coord.lat;
      var lng = coord.lng;

      console.log(
        "You clicked the map at latitude: " + lat + " and longitude: " + lng
      );
      this.getParcelById(lat, lng);
    });
  }

  render() {
    return html`
      <h4>Wskasz działkę przy użyciu myszki</h4>
      <p>Kliknij na wybrane miejsce na mapie, aby zaznaczyć działke</p>
    `;
  }
}
