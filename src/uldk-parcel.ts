import L from "leaflet";
import { html, LitElement } from "lit";
import { customElement, property, query, state } from "lit/decorators";
import { Helper } from "./Helper";
import { ULDKApi } from "./ULDKApi";

@customElement("uldk-parcel")
export class UldkMouse extends LitElement {
  @property({ type: Object }) api?: ULDKApi;

  @property({ type: Object }) map?: L.Map;

  @property({ type: Object }) geojsonLayer?: any;

  @query("#parcelId")
  parcelInput: any;

  async getParcelById(id: string) {
    const result = await this.api?.getGeoJsonFromParcelIdOrNr(id);

    const wktJSON = Helper.wktToGeoJSON(result!);
    const message = Helper.createInfoObject(result!);
    const dataJSON = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: wktJSON,
          properties: {
            popupContent: message,
            underConstruction: false,
          },
          id: 1,
        },
      ],
    };

    this.geojsonLayer.clearLayers();
    this.geojsonLayer?.addData(dataJSON);

    this.map?.fitBounds(this.geojsonLayer.getBounds(), {});

    return "";
  }

  render() {
    return html`
      <vaadin-text-field
        id="parcelId"
        label="Podaj id działki"
      ></vaadin-text-field>
      <vaadin-button
        id="searchParcelBtn"
        @click=${async (e) => {
          const parcelId = `${this.parcelInput.value}`;
          console.log(await this.getParcelById(parcelId));
        }}
        >Szukaj w ULDK</vaadin-button
      >
    `;
  }
}
