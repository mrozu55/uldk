import wellknown from "wellknown/wellknown.js";

export class Helper {
  public static wktToGeoJSON(wkt: string): GeoJSON.GeometryObject {
    return wellknown.parse(wkt);
  }

  static createInfoObject(result: string) {
    const arr = result!.split("|");
    const items: string[] = new Array();

    arr.forEach((item) => {
      items.push(item);
    });

    return `Województwo: ${items[1]} 
        Powiat: ${items[2]}
        Gmina: ${items[3]} 
        Miejscowość: ${items[4]} 
        Identyfikator działki: ${items[5]} 
        `;
  }
}
